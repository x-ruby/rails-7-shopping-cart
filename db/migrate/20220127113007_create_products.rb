class CreateProducts < ActiveRecord::Migration[7.0]
	def change
		create_table :products do |t|
			t.string :name, index: true, null: false
			t.decimal :price, precision: 10, scale: 2, null: false
			t.boolean :premium, index: true
			t.text :description

			t.timestamps
		end
	end
end
