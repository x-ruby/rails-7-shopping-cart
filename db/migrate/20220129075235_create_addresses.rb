class CreateAddresses < ActiveRecord::Migration[7.0]
	def change
		create_table :addresses do |t|
			t.string :line_1, null: false
			t.string :line_2
			t.string :city, null: false
			t.string :postcode, null: false

			t.references :customer, null: false, foreign_key: true
			t.timestamps
		end
	end
end
