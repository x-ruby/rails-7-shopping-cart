# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

unless Product.any?
	10.times do
		Product.create(
			name: FFaker::Product.product,
			price: (rand * 100).round(2),
			premium: [true, false, false].sample,
			description: FFaker::HipsterIpsum.paragraph,
		).tap do |product|
			product.hero_image.attach(io: FFaker::Image.file, filename: 'hero_image.png')
		end
	end
end
