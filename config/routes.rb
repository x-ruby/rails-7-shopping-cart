Rails.application.routes.draw do
	# Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

	# Defines the root path route ("/")
	# root "articles#index"

	resources :shopping_carts, only: [:index, :edit, :update, :destroy], path: :shopping_cart

	resources :line_items, only: [:create]

	resources :orders, only: [:new, :create, :show]
	resources :products, only: [:index, :show]

	root 'root#index'
end
