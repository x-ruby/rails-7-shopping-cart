# Rails Test Project - Simon Lennon

This is a project I was asked to build recently for as part of an interview process, it had specific technology
requirements based on the companies preferred technology stack.

Project Requirements:

* Full test coverage with Rspec
* No CSS use tailwind css only for styling
* No Javascript
* Pure Rails build with minimal additional gems
