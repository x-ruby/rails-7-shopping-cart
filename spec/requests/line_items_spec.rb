require 'rails_helper'

RSpec.describe "/line_items", type: :request do
	let(:product) { FactoryBot.create :product }
	let(:shopping_cart) { FactoryBot.build :shopping_cart }

	let(:valid_attributes) {
		{ product_id: product.id, quantity: 1 }
	}

	let(:invalid_attributes) {
		{ product_id: nil, quantity: nil }
	}

	describe "POST /create" do
		context "with valid parameters" do
			it "creates a new LineItem" do
				expect {
					post line_items_path, params: { line_item: valid_attributes }
				}.to change(LineItem, :count).by(1)
			end

			it "redirects to the created line_item" do
				post line_items_path, params: { line_item: valid_attributes }
				expect(response).to redirect_to(product_path(LineItem.last.product))
			end
		end

		context "with invalid parameters" do
			it "does not create a new LineItem" do
				expect {
					post line_items_path, params: { line_item: invalid_attributes }
				}.to change(LineItem, :count).by(0)
			end

			it "renders a successful response" do
				post line_items_path, params: { line_item: invalid_attributes }

				expect(response).to redirect_to(root_path)
			end
		end
	end
end
