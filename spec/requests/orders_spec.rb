require 'rails_helper'

RSpec.describe "/orders", type: :request do
	let(:customer) { FactoryBot.build :customer }
	let(:address) { FactoryBot.build :address }

	let(:valid_attributes) { {
		customer: {
			first_name: customer.first_name, last_name: customer.last_name, email_address: customer.email_address, phone_number: customer.phone_number
		},
		address: {
			line_1: address.line_1, line_2: address.line_2, city: address.city, postcode: address.postcode
		}
	} }

	let(:invalid_attributes) { {
		customer: {
			first_name: '', last_name: '', email_address: '', phone_number: ''
		},
		address: {
			line_1: '', line_2: '', city: '', postcode: ''
		}
	} }

	describe "GET /show" do
		it "renders a successful response" do
			post orders_path, params: { order: valid_attributes }
			get order_path(Order.last)

			expect(response).to be_successful
		end
	end

	describe "GET /new" do
		it "renders a successful response" do
			get new_order_path
			expect(response).to be_successful
		end
	end

	describe "POST /create" do
		context "with valid parameters" do
			it "creates a new Order" do
				expect {
					post orders_path, params: { order: valid_attributes }
				}.to change(Order, :count).by(1)
			end

			it "redirects to the created order" do
				post orders_path, params: { order: valid_attributes }
				expect(response).to redirect_to(order_path(Order.last))
			end
		end

		context "with invalid parameters" do
			it "does not create a new Order" do
				expect {
					post orders_path, params: { order: invalid_attributes }
				}.to change(Order, :count).by(0)
			end

			it "renders a successful response" do
				post orders_path, params: { order: invalid_attributes }

				expect(response).to have_http_status :unprocessable_entity
			end
		end
	end

end
