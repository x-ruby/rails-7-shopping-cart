require 'rails_helper'

RSpec.describe "/products", type: :request do
	let(:product) { FactoryBot.create :product }

	let(:valid_attributes) { {
		name: product.name, price: product.price, premium: product.premium, description: product.description
	} }

	let(:invalid_attributes) { {
		name: nil, price: nil, premium: nil, description: nil
	} }

	describe "GET /index" do
		it "renders a successful response" do
			product
			get products_path
			expect(response).to be_successful
		end
	end

	describe "GET /show" do
		it "renders a successful response" do
			product
			get product_path(product)
			expect(response).to be_successful
		end
	end
end
