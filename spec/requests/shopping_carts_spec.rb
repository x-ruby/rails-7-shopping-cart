require 'rails_helper'

RSpec.describe "/shopping_cart", type: :request do
	let(:product1) { FactoryBot.create :product }
	let(:product2) { FactoryBot.create :product }
	let(:session) { FactoryBot.create :session }
	let(:line_items) { FactoryBot.create_list :line_item, 2, owner: session }

	let(:invalid_attributes) {
		new_line_items = {}
		new_line_items[product1.id] = { quantity: '' }
	}

	before(:each) do
		post line_items_path, params: { line_item: { product_id: product1.id, quantity: 1 } }
		post line_items_path, params: { line_item: { product_id: product2.id, quantity: 2 } }

		@shopping_cart = ShoppingCart.last
	end

	describe "GET /index" do
		it "renders a successful response" do
			get shopping_carts_path
			expect(response).to be_successful
		end
	end

	describe "PATCH /update" do

		context "with valid parameters" do
			let(:new_quantity) { 10000 }
			let(:new_attributes) {
				new_line_items = {}
				new_line_items[product1.id] = { quantity: new_quantity }
				new_line_items[product2.id] = { destroy: 'Delete' }

				{ line_items: new_line_items }
			}

			it "updates the requested shopping_cart" do
				patch shopping_cart_path(@shopping_cart), params: { shopping_cart: new_attributes }
				@shopping_cart.reload
				@shopping_cart.line_items.reload

				expect(@shopping_cart.line_items.first.quantity).to eq new_quantity # first item new quantity
				expect(@shopping_cart.line_items.length).to eq 1 # last item removed
			end

			it "redirects to the shopping_cart" do
				patch shopping_cart_path(@shopping_cart), params: { shopping_cart: new_attributes }
				@shopping_cart.reload
				expect(response).to redirect_to(shopping_carts_url)
			end
		end

		context "with invalid parameters" do
			it "renders a successful response" do
				patch shopping_cart_path(@shopping_cart), params: { shopping_cart: invalid_attributes }
				expect(response).to have_http_status :unprocessable_entity
			end
		end
	end

	describe "DELETE /destroy" do
		it "destroys the requested shopping_cart" do
			expect {
				delete shopping_cart_path(@shopping_cart)
			}.to change(ShoppingCart, :count).by(-1)
		end

		it "redirects to the shopping_carts list" do
			delete shopping_cart_path(@shopping_cart)
			expect(response).to redirect_to(shopping_carts_path)
		end
	end
end

