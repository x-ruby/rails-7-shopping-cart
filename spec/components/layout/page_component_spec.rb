require "rails_helper"

RSpec.describe Layout::PageComponent, type: :component do
	include ComponentsHelper

	let(:session) { FactoryBot.build :session, :with_cart }

	subject {
		described_class.new(title: 'My Title', width: 'w-full', alert: 'My Alert', notice: 'My Notice', session: session)
	}

	before(:each) do
		render_inline(subject)
		@component = strip_spacing(rendered_component)
	end

	it "renders correctly" do
		expect(@component).to have_selector ".container > div", class: 'w-full'
	end

	it "has heading component" do
		expect(@component).to have_text "My Title"
	end

	it "has message component" do
		expect(@component).to have_text "My Alert"
		expect(@component).to have_text "My Notice"
	end

	it "has header component" do
		expect(@component).to have_selector ".line_items", text: session.shopping_cart.line_items.length.to_s
	end
end
