require "rails_helper"

RSpec.describe Layout::FieldComponent, type: :component do
	include ComponentsHelper

	subject {
		described_class.new(label: 'My Label', value: 'My Value')
	}

	it "renders correctly" do
		render_inline(subject)
		component = strip_spacing(rendered_component)

		expect(component).to have_text "My Label"
		expect(component).to have_text "My Value"
	end
end
