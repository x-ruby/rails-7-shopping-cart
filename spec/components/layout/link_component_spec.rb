require "rails_helper"

RSpec.describe Layout::LinkComponent, type: :component do
	include ComponentsHelper
	include ColorsHelper

	subject {
		described_class.new(url: '/some_url/', cls: 'my_class', label: 'My Label', color: :blue, type: 'a', profile: :header)
	}

	it "renders correctly" do
		render_inline(subject)
		component = strip_spacing(rendered_component)

		expect(component).to have_selector "a[href='/some_url/']"
		expect(component).to have_selector "a", class: 'my_class'
		expect(component).to have_selector "a", text: 'My Label'
		expect(component).to have_selector "a", class: colors(:blue, type: 'a')
		expect(component).to have_selector "a", class: 'header_link'
	end
end
