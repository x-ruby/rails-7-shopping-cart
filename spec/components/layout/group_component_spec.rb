require "rails_helper"

RSpec.describe Layout::GroupComponent, type: :component do
	include ComponentsHelper

	subject {
		described_class.new(label: 'My Label')
	}

	it "renders correctly" do
		render_inline(subject)
		component = strip_spacing(rendered_component)

		expect(component).to have_text "My Label"
	end
end
