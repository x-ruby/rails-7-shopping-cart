require "rails_helper"

RSpec.describe Layout::HeadingComponent, type: :component do
	include ComponentsHelper

	subject {
		described_class.new(text: "My Heading")
	}

	it "renders correctly" do
		render_inline(subject)
		component = strip_spacing(rendered_component)

		expect(component).to have_text "My Heading"
	end
end
