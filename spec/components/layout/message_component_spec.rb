require "rails_helper"

RSpec.describe Layout::MessageComponent, type: :component do
	include ComponentsHelper

	subject {
		described_class.new(notice: nil, alert: nil)
	}

	it "renders blank" do
		render_inline(subject)
		expect(rendered_component).to eq ""
	end

	it "renders notice" do
		render_inline(described_class.new(notice: 'My Notice', alert: nil))
		component = strip_spacing(rendered_component)

		expect(component).to have_text "My Notice"
		expect(component).to have_selector "#notice"
	end

	it "renders alert" do
		render_inline(described_class.new(notice: nil, alert: 'My Alert'))
		component = strip_spacing(rendered_component)

		expect(component).to have_text "My Alert"
		expect(component).to have_selector "#alert"
	end
end
