require "rails_helper"

RSpec.describe Layout::HeaderComponent, type: :component do
	include ComponentsHelper
	include RSpec::Rails::ViewExampleGroup

	let(:session) { FactoryBot.build :session, :with_cart }

	subject {
		described_class.new(width: 'w-full', session: session)
	}

	it "renders correctly" do
		render_inline(subject)
		component = strip_spacing(rendered_component)

		expect(component).to have_selector "a", text: 'Products'
		expect(component).to have_selector "a", text: 'Shopping Cart'

		expect(component).to have_selector ".line_items", text: session.shopping_cart.line_items.length.to_s

		total = 0
		session.shopping_cart.line_items.each do |item|
			total += item.quantity * item.product.price
		end

		expect(component).to have_selector ".total", text: number_to_currency(total)
	end
end
