require "rails_helper"

RSpec.describe Form::ErrorComponent, type: :component do
	include ComponentsHelper

	let(:address) { FactoryBot.build :address }

	subject {
		address.line_1 = nil
		address.valid?

		described_class.new(model: address)
	}

	it "renders blank" do
		render_inline(described_class.new(model: address))
		expect(rendered_component).to eq ""
	end

	it "renders with error message" do
		render_inline(subject)

		component = strip_spacing(rendered_component)

		expect(component).to have_text "1 error prohibited this Address from being saved"
		expect(component).to have_text "can't be blank"
	end

end
