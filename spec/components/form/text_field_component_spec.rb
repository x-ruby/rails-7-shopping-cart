require "rails_helper"

RSpec.describe Form::TextFieldComponent, type: :component do
	include ComponentsHelper
	include ComponentsHelper::Form

	let(:address) { FactoryBot.build :address }

	subject {
		described_class.new(form: form(address), name: :line_1)
	}

	it "renders correctly" do
		render_inline(subject)

		component = strip_spacing(rendered_component)

		expect(component).to have_selector "input[name='line_1']"
	end
end
