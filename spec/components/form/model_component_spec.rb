require "rails_helper"

RSpec.describe Form::ModelComponent, type: :component do
	include ComponentsHelper
	include ComponentsHelper::Form

	let(:address) { FactoryBot.build :address }

	subject {
		described_class.new(record_name: 'something[]', model: address, title: 'My Title')
	}

	it "renders correctly" do
		render_inline(subject)
		component = strip_spacing(rendered_component)

		expect(component).to have_text "My Title"
	end

	it "renders text field correctly" do
		form = form(address)
		render_inline(subject) do |address|
			render_inline(address.text(form: form, name: :line_1))

			component = strip_spacing(rendered_component)
			expect(component).to have_selector "input[name='line_1']"
		end
	end
end
