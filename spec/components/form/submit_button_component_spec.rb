require "rails_helper"

RSpec.describe Form::SubmitButtonComponent, type: :component do
	include ComponentsHelper
	include ComponentsHelper::Form
	include ColorsHelper

	let(:address) { FactoryBot.build :address }

	subject {
		described_class.new(form: form(address), name: :button_name, cls: 'button_class', label: 'button label', color: :blue)
	}

	it "renders correctly" do
		render_inline(subject)

		component = strip_spacing(rendered_component)

		expect(component).to have_selector "input"
		expect(component).to have_selector "input", class: 'button_class'
		expect(component).to have_selector "input", class: colors(:blue, type: :button)
		expect(component).to have_selector "input[value='button label']"
		expect(component).to have_selector "input[name='button_name']"
	end
end
