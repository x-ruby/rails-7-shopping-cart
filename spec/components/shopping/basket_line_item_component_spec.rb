require "rails_helper"

RSpec.describe Shopping::BasketLineItemComponent, type: :component do
	include ComponentsHelper
	include ComponentsHelper::Form

	let(:line_item) { FactoryBot.create :line_item, :shopping_cart }

	subject {
		described_class.new(line_item: line_item, form: form(line_item))
	}

	it "renders correctly" do
		render_inline(subject)

		component = strip_spacing(rendered_component)
		product = line_item.product

		expect(component).to have_selector "#quantity[value='#{line_item.quantity}']"
		expect(component).to have_selector '.product_name', text: product.name
		expect(component).to have_selector "input[name='shopping_cart[line_items][#{product.id}][destroy]']"
	end
end
