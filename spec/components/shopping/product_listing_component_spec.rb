require "rails_helper"

RSpec.describe Shopping::ProductListingComponent, type: :component do
	include ComponentsHelper
	include RSpec::Rails::ViewExampleGroup

	let(:product) { FactoryBot.create :product }
	let(:line_item) { FactoryBot.build :line_item }

	subject {
		product.premium = true
		described_class.new(product: product, line_item: line_item)
	}

	it "renders correctly" do
		render_inline(subject)
		component = strip_spacing(rendered_component)

		expect(component).to have_selector ".product_image img"
		expect(component).to have_selector ".product_name > a", text: product.name
		expect(component).to have_selector ".product_premium"
		expect(component).to have_selector ".product_price", text: number_to_currency(product.price)
		expect(component).to have_selector ".product_description", text: product.description

		expect(component).to have_selector "input[value='Add to Basket']"
	end
end
