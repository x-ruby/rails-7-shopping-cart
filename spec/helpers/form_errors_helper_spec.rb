require 'rails_helper'

RSpec.describe FormErrorsHelper, type: :helper do
	let(:address) { FactoryBot.build :address }

	describe "error_message_on" do
		it "returns no errors" do
			message = helper.error_message_on(address, :line_1)
			expect(message).to eq nil
		end

		it "returns error" do
			address.line_1 = nil
			address.valid?
			message = helper.error_message_on(address, :line_1)

			expect(message).to have_selector 'div', text: "can't be blank"
		end
	end
end
