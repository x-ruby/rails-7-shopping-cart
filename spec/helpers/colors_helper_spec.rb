require 'rails_helper'

RSpec.describe ColorsHelper, type: :helper do
	describe "colors" do
		it "has text color" do
			color = helper.colors(:blue, type: 'text')
			expect(color).to match /^text\-/
		end

		it "has button color" do
			color = helper.colors(:blue, type: 'button')
			expect(color).to match /^bg\-/
		end

		it "does not have color" do
			expect { helper.colors(:not_a_color) }.to raise_error StandardError, "Unknown color: not_a_color"
		end
	end
end
