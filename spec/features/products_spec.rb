require 'rails_helper'

RSpec.describe 'Products', type: :feature do
	let(:products) { FactoryBot.create_list :product, 3 }

	before { products }

	describe "#index" do
		before do
			visit products_path
		end

		it 'shows a list of products' do
			expect(page).to have_selector '.product', count: 3
		end

		describe 'shows products' do
			it_should_behave_like "product shown correctly", '#products > form', 1
			it_should_behave_like "product shown correctly", '#products > form', 2
			it_should_behave_like "product shown correctly", '#products > form', 3
		end

		describe 'products add to basket' do
			it_should_behave_like "product added to basket", '#products > form', 1
			it_should_behave_like "product added to basket", '#products > form', 2
			it_should_behave_like "product added to basket", '#products > form', 3
		end
	end

	describe "#show" do
		before do
			visit product_path(products[0].id)
		end

		it_should_behave_like "product shown correctly", 'main', 1

		it_should_behave_like "product added to basket", 'main', 1
	end
end
