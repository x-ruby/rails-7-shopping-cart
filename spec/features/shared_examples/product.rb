require 'rails_helper'

RSpec.configure do |c|
	c.include ActionView::Helpers::NumberHelper
end

RSpec.shared_examples "product shown correctly" do |selector, item|
	def product(item)
		products[item - 1]
	end

	def product_item(selector, item)
		product_list = page.all(selector)
		product_list[item - 1]
	end

	describe "product #{item}" do
		before do
			@line_item = product_item(selector, item)
		end

		it "shows product photo" do
			img = @line_item.find('.product_image img')

			visit img[:src]
			expect(page.status_code).to be 200
		end

		it "shows product name" do
			expect(@line_item).to have_selector '.product_name', text: product(item).name, count: 1
		end

		it "shows product description" do
			expect(@line_item).to have_selector '.product_description', text: product(item).description, count: 1
		end

		it "shows product price" do
			expect(@line_item).to have_selector '.product_price', text: number_to_currency(product(item).price), count: 1
		end

		it "shows premium or not" do
			if product(item).premium
				expect(@line_item).to have_selector '.product_premium', count: 1
			end
		end

		it "shows add to basket button" do
			expect(@line_item).to have_selector "input[value='Add to Basket']", count: 1
		end
	end
end

RSpec.shared_examples 'product added to basket' do |selector, item|
	def product(item)
		products[item - 1]
	end

	def product_item(selector, item)
		product_list = page.all(selector)
		product_list[item - 1]
	end

	describe "product #{item}" do

		describe "product page" do
			it "adds to basket" do
				button = product_item(selector, item).find ".product_add_to_basket > input[type='submit']"
				expect {
					button.click
				}.to change(LineItem, :count).by(1)

				expect(page).to have_selector '#notice', text: 'Product added to your basket'
			end
		end

		describe "basket page" do
			before do
				button = product_item(selector, item).find ".product_add_to_basket > input[type='submit']"
				expect {
					button.click
				}.to change(LineItem, :count).by(1)
				expect(page).to have_selector '#notice', text: 'Product added to your basket'

				visit shopping_carts_path

				@line_item = page.find('.line_item')
			end

			it "shows basket quantity" do
				expect(@line_item).to have_field class: 'quantity', with: '1', count: 1
			end

			it "shows product name" do
				expect(@line_item).to have_selector '.product_name', text: product(item).name, count: 1
			end

			it "shows remove button" do
				expect(@line_item).to have_selector "input[value='Remove']", count: 1
			end
		end
	end
end
