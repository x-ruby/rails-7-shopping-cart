require 'rails_helper'

RSpec.describe 'Shopping Cart', type: :feature do
	let(:products) { FactoryBot.create_list :product, 3 }

	before { products }

	before(:each) do
		visit products_path
	end

	require 'rails_helper'

	describe '#index' do
		before do
			visit products_path
		end

		describe 'products add to basket' do
			it_should_behave_like "product added to basket", '#products > form', 1
		end
	end

	describe '#update' do
		before do
			visit product_path(products.first)

			button = page.find ".product_add_to_basket > input[type='submit']"
			button.click
			expect(page).to have_selector '#notice', text: 'Product added to your basket'

			visit shopping_carts_path

			@line_item = page.find('.line_item')
		end

		it 'line item updated in basket' do
			@line_item.find('input.quantity')
			@line_item.fill_in with: '2'

			click_button 'Update'

			expect(page).to have_selector '#notice', text: 'Shopping cart was successfully updated.', count: 1

			expect(page).to have_field class: 'quantity', with: '2', count: 1
		end

		it 'line item with invalid quantity' do
			@line_item.find('input.quantity')
			@line_item.fill_in with: ''

			click_button 'Update'
			expect(page).to have_selector '#error_explanation > h2', text: '1 error prohibited this Shopping Cart from being saved:'
			expect(page).to have_selector '#error_explanation > ul > li', text: 'Line items is invalid'
		end
	end

	describe '#destroy' do
		before do
			visit product_path(products.first)

			button = page.find ".product_add_to_basket > input[type='submit']"

			expect {
				button.click
			}.to change(LineItem, :count).by(1)
			expect(page).to have_selector '#notice', text: 'Product added to your basket'

			visit shopping_carts_path

			@line_item = page.find('.line_item')
		end

		it 'removes the line item' do
			@line_item.click_button 'Remove'
			expect(page).to have_selector '#notice', text: 'Shopping cart was successfully updated.', count: 1
			expect(page).to have_selector '.cart_empty', count: 1
		end
	end
end
