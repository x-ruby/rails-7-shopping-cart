require 'rails_helper'

RSpec.describe 'Orders', type: :feature do
	let(:products) { FactoryBot.create_list :product, 3 }
	let(:customer) { FactoryBot.build :customer }
	let(:address) { FactoryBot.build :address }
	let(:session) { FactoryBot.build :session }

	before { products }

	describe "#new" do
		before do
			visit product_path(products.first)

			button = page.find ".product_add_to_basket > input[type='submit']"
			expect {
				button.click
			}.to change(LineItem, :count).by(1)
			expect(page).to have_selector '#notice', text: 'Product added to your basket'

			visit shopping_carts_path

			click_link 'Proceed to Checkout'
			expect(page).to have_selector 'h1', text: 'Checkout'
		end

		it "creates valid order" do
			fill_in id: 'order_customer_first_name', with: customer.first_name
			fill_in id: 'order_customer_last_name', with: customer.last_name
			fill_in id: 'order_customer_email_address', with: customer.email_address
			fill_in id: 'order_customer_phone_number', with: customer.phone_number

			fill_in id: 'order_address_line_1', with: address.line_1
			fill_in id: 'order_address_line_2', with: address.line_2
			fill_in id: 'order_address_city', with: address.city
			fill_in id: 'order_address_postcode', with: address.postcode

			expect {
				click_on 'Place Order'
			}.to change(Order, :count).by(1)

			expect(page).to have_selector '#notice', text: 'Order was successfully created.'

			expect(page).to have_selector '.first_name > span', text: customer.first_name
			expect(page).to have_selector '.last_name > span', text: customer.last_name
			expect(page).to have_selector '.email_address > span', text: customer.email_address
			expect(page).to have_selector '.phone_number > span', text: customer.phone_number
			expect(page).to have_selector '.line_1 > span', text: address.line_1
			expect(page).to have_selector '.line_2 > span', text: address.line_2
			expect(page).to have_selector '.city > span', text: address.city
			expect(page).to have_selector '.postcode > span', text: address.postcode
		end

		describe 'Validations' do
			before do
				fill_in id: 'order_customer_first_name', with: customer.first_name
				fill_in id: 'order_customer_last_name', with: customer.last_name
				fill_in id: 'order_customer_email_address', with: customer.email_address
				fill_in id: 'order_customer_phone_number', with: customer.phone_number

				fill_in id: 'order_address_line_1', with: address.line_1
				fill_in id: 'order_address_line_2', with: address.line_2
				fill_in id: 'order_address_city', with: address.city
				fill_in id: 'order_address_postcode', with: address.postcode
			end

			context "customer" do
				it 'is not valid without first name' do
					fill_in id: 'order_customer_first_name', with: ''
					expect {
						click_on 'Place Order'
					}.to change(Order, :count).by(0)

					expect(page).to have_selector '#error_explanation > h2', text: '1 error prohibited this Order from being saved:', count: 1
					expect(page).to have_selector '#error_explanation > ul > li', text: 'Customer details are invalid', count: 1

					expect(page).to have_selector ".field_error[for='first_name']", text: "can't be blank", count: 1
				end

				it 'is not valid without last name' do
					fill_in id: 'order_customer_last_name', with: ''
					expect {
						click_on 'Place Order'
					}.to change(Order, :count).by(0)

					expect(page).to have_selector '#error_explanation > h2', text: '1 error prohibited this Order from being saved:', count: 1
					expect(page).to have_selector '#error_explanation > ul > li', text: 'Customer details are invalid', count: 1

					expect(page).to have_selector ".field_error[for='last_name']", text: "can't be blank", count: 1
				end

				it 'is not valid without email address' do
					fill_in id: 'order_customer_email_address', with: ''
					expect {
						click_on 'Place Order'
					}.to change(Order, :count).by(0)

					expect(page).to have_selector '#error_explanation > h2', text: '1 error prohibited this Order from being saved:', count: 1
					expect(page).to have_selector '#error_explanation > ul > li', text: 'Customer details are invalid', count: 1

					expect(page).to have_selector ".field_error[for='email_address']", text: "can't be blank", count: 1
				end

				it 'is not valid without phone number' do
					fill_in id: 'order_customer_phone_number', with: ''
					expect {
						click_on 'Place Order'
					}.to change(Order, :count).by(0)

					expect(page).to have_selector '#error_explanation > h2', text: '1 error prohibited this Order from being saved:', count: 1
					expect(page).to have_selector '#error_explanation > ul > li', text: 'Customer details are invalid', count: 1

					expect(page).to have_selector ".field_error[for='phone_number']", text: "can't be blank, must be a valid UK Phone Number", count: 1
				end
			end

			context "address" do
				it 'is not valid without line 1' do
					fill_in id: 'order_address_line_1', with: ''
					expect {
						click_on 'Place Order'
					}.to change(Order, :count).by(0)

					expect(page).to have_selector '#error_explanation > h2', text: '1 error prohibited this Order from being saved:', count: 1
					expect(page).to have_selector '#error_explanation > ul > li', text: 'Address details are invalid', count: 1

					expect(page).to have_selector ".field_error[for='line_1']", text: "can't be blank", count: 1
				end

				it 'is not valid without city' do
					fill_in id: 'order_address_city', with: ''
					click_on 'Place Order'

					expect(page).to have_selector '#error_explanation > h2', text: '1 error prohibited this Order from being saved:', count: 1
					expect(page).to have_selector '#error_explanation > ul > li', text: 'Address details are invalid', count: 1

					expect(page).to have_selector ".field_error[for='city']", text: "can't be blank", count: 1
				end

				it 'is not valid without postcode' do
					fill_in id: 'order_address_postcode', with: ''
					expect {
						click_on 'Place Order'
					}.to change(Order, :count).by(0)

					expect(page).to have_selector '#error_explanation > h2', text: '1 error prohibited this Order from being saved:', count: 1
					expect(page).to have_selector '#error_explanation > ul > li', text: 'Address details are invalid', count: 1

					expect(page).to have_selector ".field_error[for='postcode']", text: "can't be blank", count: 1
				end
			end
		end
	end

	describe "#show" do
		before do
			visit product_path(products.first)

			button = page.find ".product_add_to_basket > input[type='submit']"
			expect {
				button.click
			}.to change(LineItem, :count).by(1)
			expect(page).to have_selector '#notice', text: 'Product added to your basket'

			visit shopping_carts_path

			click_link 'Proceed to Checkout'
			expect(page).to have_selector 'h1', text: 'Checkout'

			fill_in id: 'order_customer_first_name', with: customer.first_name
			fill_in id: 'order_customer_last_name', with: customer.last_name
			fill_in id: 'order_customer_email_address', with: customer.email_address
			fill_in id: 'order_customer_phone_number', with: customer.phone_number

			fill_in id: 'order_address_line_1', with: address.line_1
			fill_in id: 'order_address_line_2', with: address.line_2
			fill_in id: 'order_address_city', with: address.city
			fill_in id: 'order_address_postcode', with: address.postcode

			expect {
				click_on 'Place Order'
			}.to change(Order, :count).by(1)

			expect(page).to have_selector '#notice', text: 'Order was successfully created.'
		end

		it "shows order" do
			expect(page).to have_selector '.first_name > span', text: customer.first_name
			expect(page).to have_selector '.last_name > span', text: customer.last_name
			expect(page).to have_selector '.email_address > span', text: customer.email_address
			expect(page).to have_selector '.phone_number > span', text: customer.phone_number
			expect(page).to have_selector '.line_1 > span', text: address.line_1
			expect(page).to have_selector '.line_2 > span', text: address.line_2
			expect(page).to have_selector '.city > span', text: address.city
			expect(page).to have_selector '.postcode > span', text: address.postcode
		end
	end
end
