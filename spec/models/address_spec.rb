require 'rails_helper'

RSpec.describe Address, type: :model do
	let(:customer) { FactoryBot.build :customer }

	subject {
		described_class.new(
			customer: customer,
			line_1: '123 Four Street',
			city: 'Bournemouth',
			postcode: 'BH1 1AA',
		)
	}

	it 'is valid', kind: :valid do
		expect(subject).to be_valid
		expect(subject.line_1).to eq '123 Four Street'
		expect(subject.city).to eq 'Bournemouth'
		expect(subject.postcode).to eq 'BH11AA' # no spaces
	end

	describe 'has_fields', kind: :valid do
		it 'has premium' do
			subject.line_2 = '456 Seven Street'
			expect(subject.line_2).to eq '456 Seven Street'
		end
	end

	context 'Validations', group: :validation do
		it 'is not valid without street 1' do
			subject.line_1 = nil
			expect(subject).to_not be_valid

			expect(subject.errors[:line_1]).to eq ["can't be blank"]
		end

		it 'is not valid without city' do
			subject.city = nil
			expect(subject).to_not be_valid

			expect(subject.errors[:city]).to eq ["can't be blank"]
		end

		it 'is not valid without postcode' do
			subject.postcode = nil
			expect(subject).to_not be_valid

			expect(subject.errors[:postcode]).to eq ["can't be blank"]
		end
	end

	context 'Associations', group: :associations do
		it "belongs to customer" do
			should belong_to(:customer)
		end

		it "has many orders" do
			should have_many(:orders)
		end
	end
end
