require 'rails_helper'

RSpec.describe Product, type: :model do
	subject {
		described_class.new(
			name: 'product_name',
			price: 123.45
		)
	}

	it 'is valid with attributes', kind: :valid do
		expect(subject).to be_valid
		expect(subject.name).to eq 'product_name'
		expect(subject.price).to eq 123.45
	end

	describe 'has_fields', kind: :valid do
		it 'has premium' do
			subject.premium = true
			expect(subject.premium).to eq true
		end

		it 'has description' do
			subject.description = 'description'
			expect(subject.description).to eq 'description'
		end
	end

	context 'Validations', group: :validation, kind: :invalid do

		it 'is not valid without name' do
			subject.name = nil
			expect(subject).to_not be_valid

			expect(subject.errors[:name]).to eq ["can't be blank"]
		end

		it 'is not valid without price' do
			subject.price = nil
			expect(subject).to_not be_valid

			expect(subject.errors[:price]).to eq ["can't be blank"]
		end
	end

	context 'Associations', group: :associations do
		it "has many line_items" do
			should have_many(:line_items)
		end
	end
end
