require 'rails_helper'

RSpec.describe Customer, type: :model do
	subject {
		described_class.new(
			first_name: 'first_name',
			last_name: 'last_name',
			email_address: 'me@my.com',
			phone_number: '02123 123456',
		)
	}

	it 'is valid with attributes', kind: :valid do
		expect(subject).to be_valid
		expect(subject.first_name).to eq 'first_name'
		expect(subject.last_name).to eq 'last_name'
		expect(subject.email_address).to eq 'me@my.com'
		expect(subject.phone_number).to eq '02123123456' # removes spaces
	end

	context 'Validations', group: :validation do

		context 'Valid', kind: :valid do
			[
				"020 1234 5678", "020 12345678", "02012345678", "+44 20 1234 5678", "+44 20 12345678", "+4420 12345678", "+442012345678", # London (3 digit area code)
				"0141 123 4567", "0141 1234567", "01411234567", "+44 141 423 8297", "+44 141 4238297", "+44141 4238297", "+441414238297", # Glasgow (4 digit area code)
				"01223 123 456", "01223 123 456", "01223 123 456", "+44 1223 123 456", "+44 1223 123456", "+441223 123456", "+441223123456" # Cambridge (5 digit area code)
			].each do |number|
				it "#{number.to_s} is valid phone number" do
					subject.phone_number = number
					expect(subject).to be_valid
				end
			end
		end

		context 'Invalid', kind: :invalid do
			it 'is not valid without first name' do
				subject.first_name = nil
				expect(subject).to_not be_valid

				expect(subject.errors[:first_name]).to eq ["can't be blank"]
			end

			it 'is not valid without last name' do
				subject.last_name = nil
				expect(subject).to_not be_valid

				expect(subject.errors[:last_name]).to eq ["can't be blank"]
			end

			it 'is not valid without email address' do
				subject.email_address = nil
				expect(subject).to_not be_valid

				expect(subject.errors[:email_address]).to eq ["can't be blank", "is invalid"]
			end

			it 'is invalid email address' do
				subject.email_address = 'affddfddffd@sss'
				expect(subject).to_not be_valid

				expect(subject.errors[:email_address]).to eq ["is invalid"]
			end

			it 'is not valid without phone number' do
				subject.phone_number = nil
				expect(subject).to_not be_valid

				expect(subject.errors[:phone_number]).to eq ["can't be blank", 'must be a valid UK Phone Number']
			end

			["02 83838 74", "+44 88282 82", "+32 20 1234 5678"].each do |number|
				it "#{number.to_s} is invalid phone number" do
					subject.phone_number = number
					expect(subject).to_not be_valid

					expect(subject.errors[:phone_number]).to eq ['must be a valid UK Phone Number']
				end
			end
		end
	end

	context 'Associations', group: :associations do
		it "has one session" do
			should have_one(:session)
		end

		it "has one address" do
			should have_one(:address)
		end

		it "has many orders" do
			should have_many(:orders)
		end
	end
end
