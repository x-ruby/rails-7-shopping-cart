require 'rails_helper'

RSpec.describe Order, type: :model do
	let(:customer) { FactoryBot.build :customer }
	let(:address) { FactoryBot.build :address }

	subject {
		described_class.new(
			customer: customer,
			address: address,
		)
	}

	it 'is valid', kind: :valid do
		expect(subject).to be_valid
	end

	context 'Associations', group: :associations do
		it "belongs to customer" do
			should belong_to(:customer)
		end

		it "belongs to address" do
			should belong_to(:address)
		end

		it "has many orders" do
			should have_many(:line_items)
		end
	end
end
