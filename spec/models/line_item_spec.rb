require 'rails_helper'

RSpec.describe LineItem, type: :model do
	let(:product) { FactoryBot.build :product }
	let(:shopping_cart) { FactoryBot.build :shopping_cart }

	subject {
		described_class.new(
			product: product,
			owner: shopping_cart,
			quantity: 1,
		)
	}

	it 'is valid', kind: :valid do
		expect(subject).to be_valid
	end

	context 'Validations', group: :validation, kind: :invalid do
		it 'is not valid without quantity' do
			subject.quantity = nil
			expect(subject).to_not be_valid

			expect(subject.errors[:quantity]).to eq ["can't be blank"]
		end
	end

	context 'Associations', group: :associations do
		it "belongs to product" do
			should belong_to(:product)
		end

		it "belongs to owner" do
			should belong_to(:owner)
		end

		it "belongs to order" do
			should belong_to(:order).without_validating_presence
		end

		it "belongs to shopping_cart" do
			should belong_to(:shopping_cart).without_validating_presence
		end
	end
end
