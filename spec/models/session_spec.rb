require 'rails_helper'

RSpec.describe Session, type: :model do
	subject {
		described_class.new
	}

	it 'is valid', kind: :valid do
		expect(subject).to be_valid
	end

	context 'Associations', group: :associations do
		it "belongs to customer" do
			should belong_to(:customer).without_validating_presence
		end

		it "have one shopping_cart" do
			should have_one(:shopping_cart)
		end
	end
end
