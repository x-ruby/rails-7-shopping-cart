require 'rails_helper'

RSpec.describe ShoppingCart, type: :model do
	let(:session) { FactoryBot.build :session }

	subject {
		described_class.new(
			session: session
		)
	}

	it 'is valid with attributes', kind: :valid do
		expect(subject).to be_valid
	end

	context 'Associations', group: :associations do
		it "belongs to customer" do
			should belong_to(:session)
		end

		it "has many orders" do
			should have_many(:line_items)
		end
	end
end
