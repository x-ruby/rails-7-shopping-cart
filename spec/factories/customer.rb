FactoryBot.define do
	factory :customer do
		first_name { FFaker::Name.first_name }
		last_name { FFaker::Name.last_name }
		email_address { FFaker::Internet.email }
		phone_number { "01234123456" } # FFaker phone number does not generate valid uk phone numbers
	end
end
