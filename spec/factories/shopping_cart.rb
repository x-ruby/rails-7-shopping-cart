FactoryBot.define do
	factory :shopping_cart do |f|

		session

		trait :with_items do
			line_items { build_list(:line_item, 3) }
		end
	end
end
