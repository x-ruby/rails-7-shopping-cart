FactoryBot.define do
	factory :product do
		name { FFaker::Product.product }
		price { (rand * 100).round(2) }
		premium { [true, false, false].sample }
		description { FFaker::HipsterIpsum.paragraph }

		after :build do |product|
			product.hero_image.attach(io: FFaker::Image.file, filename: 'hero_image.png')
		end
	end
end
