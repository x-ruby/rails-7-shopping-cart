FactoryBot.define do
	factory :session do

		customer

		trait :with_cart do
			shopping_cart { build :shopping_cart, :with_items }
		end
	end
end
