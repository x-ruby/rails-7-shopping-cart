FactoryBot.define do
	factory :line_item do
		quantity { rand(1..50) }

		product

		trait :shopping_cart do
			association :owner, factory: :shopping_cart
		end

		trait :session do
			association :owner, factory: :session
		end

		trait :zero_quantity do
			quantity { 0 }
		end
	end
end
