FactoryBot.define do
	factory :address do
		line_1 { FFaker::AddressUK.street_address }
		line_2 { FFaker::AddressUK.street_address }
		city { FFaker::AddressUK.city }
		postcode { FFaker::AddressUK.postcode }

		customer
	end
end
