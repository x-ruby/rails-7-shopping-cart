require "rails_helper"

RSpec.describe ShoppingCartsController, type: :routing do
	describe "routing" do
		it "routes to #index" do
			expect(get: "/shopping_cart").to route_to("shopping_carts#index")
		end

		it "routes to #update via PUT" do
			expect(put: "/shopping_cart/1").to route_to("shopping_carts#update", id: "1")
		end

		it "routes to #update via PATCH" do
			expect(patch: "/shopping_cart/1").to route_to("shopping_carts#update", id: "1")
		end

		it "routes to #destroy" do
			expect(delete: "/shopping_cart/1").to route_to("shopping_carts#destroy", id: "1")
		end
	end
end
