require 'rails_helper'

RSpec.describe "shopping_carts/index", type: :view do
	let(:session) { FactoryBot.create :session, :with_cart }

	it "renders a list of products in shopping carts" do
		assign(:session, session)
		assign(:shopping_cart, session.shopping_cart)

		render

		assert_select "h1", text: "Shopping Cart", count: 1

		assert_select "a", text: 'Proceed to Checkout', count: 1

		assert_select "input[value='Remove']", count: 3

		session.shopping_cart.line_items.each do |line_item|
			line = "#line_item_#{line_item.id}"
			assert_select "#{line} a.product_name", text: line_item.product.name, count: 1
			assert_select "#{line} input.quantity", value: line_item.quantity, count: 1
		end

		assert_select "input[value='Update']", count: 1
		assert_select "input[value='Remove All']", count: 1
	end

	it "renders an empty shopping cart" do
		assign(:session, session)

		render

		assert_select "h1", text: "Shopping Cart", count: 1
		assert_select "div", text: "Your shopping cart is empty", count: 1
		assert_select "a", text: 'Back to Shopping', count: 1
	end
end
