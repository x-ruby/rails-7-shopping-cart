require 'rails_helper'

RSpec.describe "products/index", type: :view do
	let(:product1) { FactoryBot.create :product }
	let(:product2) { FactoryBot.create :product }

	before(:each) do
		assign(:products, [
			product1.tap { |product|
				product.hero_image.attach(io: FFaker::Image.file, filename: 'hero_image.png')
			},
			product2.tap { |product|
				product.hero_image.attach(io: FFaker::Image.file, filename: 'hero_image.png')
			}
		])
	end

	it "renders a list of products" do
		render
		assert_select "h1", text: "Products", count: 1

		assert_select ".product_image img", count: 2

		assert_select ".product_name > a", text: product1.name, count: 1
		assert_select ".product_price", text: number_to_currency(product1.price), count: 1
		if product1.premium
			assert_select ".product_premium", count: 1
		end
		assert_select ".product_description", text: product1.description, count: 1

		assert_select ".product_name > a", text: product2.name, count: 1
		assert_select ".product_price", text: number_to_currency(product2.price), count: 1
		if product2.premium
			assert_select ".product_premium", count: 1
		end
		assert_select ".product_description", text: product2.description, count: 1

		assert_select "input[value='Add to Basket']", count: 2
	end
end
