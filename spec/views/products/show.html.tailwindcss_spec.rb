require 'rails_helper'

RSpec.describe "products/show", type: :view do
	let(:product) { FactoryBot.create :product }

	before(:each) do
		assign(:product, product.tap { |product|
			product.hero_image.attach(io: FFaker::Image.file, filename: 'hero_image.png')
			product.premium = true
		})

		assign(:line_item, LineItem.new)
	end

	it "renders attributes" do
		render
		assert_select "h1", text: "Product", count: 1

		assert_select ".product_image img", count: 1

		assert_select ".product_name > a", text: product.name, count: 1
		assert_select ".product_price", text: number_to_currency(product.price), count: 1
		assert_select ".product_premium", count: 1
		assert_select ".product_description", text: product.description, count: 1

		assert_select "input[value='Add to Basket']", count: 1

		assert_select "a", text: 'Back to products', count: 1
	end
end
