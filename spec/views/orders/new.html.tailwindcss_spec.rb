require 'rails_helper'

RSpec.describe "orders/new", type: :view do
	let(:session) { FactoryBot.build :session, :with_cart }
	let(:order) { FactoryBot.build :order }
	let(:session_path) { '' }

	it "renders an empty checkout form" do
		assign(:session, session)
		assign(:order, order)

		render

		assert_select "h1", text: "Checkout", count: 1

		assert_select "#order_customer_first_name", value: order.customer.first_name, count: 1
		assert_select "#order_customer_last_name", value: order.customer.last_name, count: 1
		assert_select "#order_customer_email_address", value: order.customer.email_address, count: 1
		assert_select "#order_customer_phone_number", value: order.customer.phone_number, count: 1

		assert_select "#order_address_line_1", value: order.address.line_1, count: 1
		assert_select "#order_address_line_2", value: order.address.line_2, count: 1
		assert_select "#order_address_city", value: order.address.city, count: 1
		assert_select "#order_address_postcode", value: order.address.postcode, count: 1

		assert_select "a", text: 'Back to shopping cart', count: 1
		assert_select "input[value='Place Order']", count: 1
	end
end
