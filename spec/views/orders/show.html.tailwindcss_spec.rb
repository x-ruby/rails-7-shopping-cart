require 'rails_helper'

RSpec.describe "orders/show", type: :view do
	let(:session) { FactoryBot.create :session, :with_cart }
	let(:order) { FactoryBot.create :order }

	before(:each) do
		session.customer = order.customer

		assign(:session, session)
		assign(:order, order)
	end

	it "renders attributes in <p>" do
		render

		assert_select "h1", text: "Order Confirmation", count: 1

		assert_select "h2", text: "Customer", count: 1
		assert_select ".first_name > span", text: order.customer.first_name, count: 1
		assert_select ".last_name > span", text: order.customer.last_name, count: 1
		assert_select ".email_address > span", text: order.customer.email_address, count: 1
		assert_select ".phone_number > span", text: order.customer.phone_number, count: 1

		assert_select "h2", text: "Address", count: 1
		assert_select ".line_1 > span", text: order.address.line_1, count: 1
		assert_select ".line_2 > span", text: order.address.line_2, count: 1
		assert_select ".city > span", text: order.address.city, count: 1
		assert_select ".postcode > span", text: order.address.postcode, count: 1

	end
end

