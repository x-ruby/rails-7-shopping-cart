# frozen_string_literal: true

class Form::SubmitButtonComponent < ViewComponent::Base
	delegate :colors, to: :helpers

	def initialize(form:, name: nil, cls: '', label: nil, color: :blue)
		@form = form
		@name = name
		@cls = cls
		@label = label
		@color = color
	end

end
