# frozen_string_literal: true

class Form::ModelComponent < ViewComponent::Base
	renders_one :group, Layout::GroupComponent
	renders_one :text, Form::TextFieldComponent

	def initialize(record_name:, model:, title: '')
		@record_name = record_name
		@model = model
		@title = title.present? ? title : model.class.name.titleize
	end

	def set_form(form)
		@form = form
	end

	def form
		@form
	end
end
