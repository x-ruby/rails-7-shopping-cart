# frozen_string_literal: true

class Form::ErrorComponent < ViewComponent::Base
	def initialize(model:)
		@model = model
	end

end
