# frozen_string_literal: true

class Form::TextFieldComponent < ViewComponent::Base
	delegate :error_message_on, to: :helpers

	def initialize(form:, name:)
		@form = form
		@name = name
	end

end
