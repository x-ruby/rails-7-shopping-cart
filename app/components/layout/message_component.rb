# frozen_string_literal: true

class Layout::MessageComponent < ViewComponent::Base
	def initialize(notice:, alert:)
		@notice = notice
		@alert = alert
	end

end
