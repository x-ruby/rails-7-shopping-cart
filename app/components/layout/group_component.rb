# frozen_string_literal: true

class Layout::GroupComponent < ViewComponent::Base
  def initialize(label:)
    @label = label
  end

end
