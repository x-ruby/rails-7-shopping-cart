# frozen_string_literal: true

class Layout::PageComponent < ViewComponent::Base
	def initialize(title:, width: 'w-full', alert:, notice:, session:)
		@title = title
		@width = width
		@alert = alert
		@notice = notice
		@session = session
	end

end
