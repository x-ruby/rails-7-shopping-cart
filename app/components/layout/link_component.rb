# frozen_string_literal: true

class Layout::LinkComponent < ViewComponent::Base
	delegate :colors, to: :helpers

	def initialize(url: '', cls: '', label: '', color: nil, type: 'a', profile: nil)
		@url = url
		@cls = "cursor-pointer text-sm inline-block #{cls}"
		@color = color
		@label = label
		@type = type.to_s

		case profile
			when :header
				@cls = "header_link   hover:text-white #{@cls}"
				unless color
					@color = :teal
				end
			else
				@cls = "font-bold #{@cls}"
		end

		case @type
			when 'button'
				@cls = "py-2 px-4 rounded text-white w-full   sm:w-auto #{@cls}"
		end
	end
end
