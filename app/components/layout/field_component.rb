# frozen_string_literal: true

class Layout::FieldComponent < ViewComponent::Base
	def initialize(label:, value:, cls: '')
		@label = label
		@value = value
		@cls = cls
	end

end
