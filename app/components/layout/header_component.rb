# frozen_string_literal: true

class Layout::HeaderComponent < ViewComponent::Base
	def initialize(width:, session:)
		@width = width
		@session = session

		if @session && @session.shopping_cart && @session.shopping_cart.line_items
			@line_items = @session.shopping_cart.line_items.length

			@total = 0
			@session.shopping_cart.line_items.each do |item|
				if item.quantity
					@total += item.quantity * item.product.price
				end
			end
			@total = number_to_currency(@total)
		end
	end
end
