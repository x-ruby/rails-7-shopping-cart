# frozen_string_literal: true

class Shopping::ProductDetailedComponent < ViewComponent::Base
	def initialize(product:, line_item:)
		@product = product
		@line_item = line_item
	end

end
