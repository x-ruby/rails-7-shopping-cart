# frozen_string_literal: true

class Shopping::BasketLineItemComponent < ViewComponent::Base
	def initialize(line_item:, form:)
		@line_item = line_item
		@form = form
	end

end
