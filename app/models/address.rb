class Address < ApplicationRecord
	validates :line_1, :city, :postcode, presence: true

	def postcode=(data)
		if data.present?
			super(data.gsub(/ /, ""))
		else
			super
		end
	end

	belongs_to :customer

	has_many :orders

	validates_associated :orders
end
