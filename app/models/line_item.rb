class LineItem < ApplicationRecord
	validates :quantity, presence: true

	belongs_to :product
	belongs_to :owner, polymorphic: true

	belongs_to :order, -> { where(owner_type: Order.to_s) }, foreign_key: :owner_id, optional: true
	belongs_to :shopping_cart, -> { where(owner_type: ShoppingCart.to_s) }, foreign_key: :owner_id, optional: true
end
