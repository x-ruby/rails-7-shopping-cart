class Order < ApplicationRecord
	belongs_to :customer
	belongs_to :address

	has_many :line_items, as: :owner

	validates_associated :line_items
end
