class Product < ApplicationRecord
	validates :price, :name, presence: true

	has_one_attached :hero_image
	has_many :line_items, as: :owner

	validates_associated :line_items
end
