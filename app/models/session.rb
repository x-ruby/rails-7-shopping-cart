class Session < ApplicationRecord
	belongs_to :customer, optional: true

	has_one :shopping_cart

	validates_associated :shopping_cart
end
