class ShoppingCart < ApplicationRecord
	belongs_to :session

	has_many :line_items, as: :owner

	validates_associated :line_items
end
