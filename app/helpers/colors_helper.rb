module ColorsHelper
	def colors(color, type: 'text')
		if color.present?
			case (type)
				when 'a', 'text'
					type = 'text'

					case color
						when :blue
							return "text-blue-500"
						when :grey
							return "text-gray-500"
						when :teal
							return "text-teal-100"
						else
							raise StandardError, "Unknown color: #{color}"
					end
				when 'button', 'bg', 'submit'
					type = 'bg'

					case color
						when :blue
							return "bg-blue-500   hover:bg-blue-700"
						when :grey
							return "bg-gray-400   hover:bg-gray-500"
						else
							raise StandardError, "Unknown color: #{color}"
					end
			end
		end
	end
end
