module FormErrorsHelper
	def error_message_on(object, method)
		return unless object.respond_to?(:errors) && object.errors.include?(method)
		errors = field_errors(object, method).join(', ')

		content_tag(:div, errors, class: 'field_error   text-red-500 text-sm mt-1', for: method)
	end

	private

		def field_errors(object, method)
			object.errors[method]
		end
end
