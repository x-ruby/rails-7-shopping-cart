module SessionConcern
	extend ActiveSupport::Concern

	included do
		before_action :use_session
	end

	def use_session
		@session_id = session[:session_token]

		@session = Session.find_or_create_by(id: @session_id)

		if @session.valid?
			session[:session_token] = @session.id
		end
	end

	def has_shopping_cart
		unless @session.shopping_cart
			ShoppingCart.create(session: @session)
		end
	end

	def has_checked_out
		if @session.shopping_cart.present?
			@session.shopping_cart.delete
		end
	end
end
