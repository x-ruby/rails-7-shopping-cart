class ProductsController < ApplicationController
	before_action :set_product, only: %i[ show ]

	# GET /products or /products.json
	def index
		@products = Product.all
		@line_item = LineItem.new
	end

	# GET /products/1 or /products/1.json
	def show
		@line_item = LineItem.new
	end

	private
		
		def set_product
			@product = Product.find(params[:id])
		end

		# Only allow a list of trusted parameters through.
		def product_params
			params.require(:product).permit(:name, :price, :premium, :description)
		end
end
