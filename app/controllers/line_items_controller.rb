class LineItemsController < ApplicationController
	before_action :has_shopping_cart, only: %i[ create ]

	# POST /line_items or /line_items.json
	def create

		@line_item = LineItem
			.where(
				product_id: line_item_params[:product_id],
				owner: @session.shopping_cart)
			.first_or_create(
				product_id: line_item_params[:product_id],
				owner: @session.shopping_cart
			)

		@line_item.increment!(:quantity, line_item_params[:quantity].present? ? line_item_params[:quantity].to_i : 1)

		respond_to do |format|
			if @line_item.save
				format.html { redirect_to request.referer || product_url(Product.find(@line_item[:product_id])), notice: "Product added to your basket" }
				format.json { render :show, status: :created, location: @line_item }
			else
				redirect = request.referer || root_url

				if @line_item[:product_id].present?
					redirect = product_url(Product.find(@line_item[:product_id]))
				end

				format.html { redirect_to redirect, alert: "Product could not be added to your basket" }
				format.json { render json: @line_item.errors, status: :unprocessable_entity }
			end
		end
	end

	private

		def set_line_item
			@line_item = LineItem.find(params[:id])
		end

		def line_item_params
			params.require(:line_item).permit(:product_id, :quantity)
		end

end
