class OrdersController < ApplicationController
	before_action :set_order, only: %i[ show ]
	before_action :set_shopping_cart, only: %i[ new, create ]

	# GET /orders/1 or /orders/1.json
	def show
		unless @order.present?
			respond_to do |format|
				format.html { redirect_to shopping_carts_url, notice: "Order not found." }
				format.json { render :show, status: :unprocessable_entity }
			end
		end
	end

	# GET /orders/new
	def new
		@order = Order.new(
			customer: @session.customer ? @session.customer : Customer.new,
			address: @session.customer&.address ? @session.customer.address : Address.new,
		)
	end

	# POST /orders or /orders.json
	def create
		create_order_params = order_params.permit(customer: [:first_name, :last_name, :email_address, :phone_number, :id], address: [:line_1, :line_2, :city, :postcode, :id])

		result = CreateOrder.run(
			session: @session,
			customer: create_order_params[:customer],
			address: create_order_params[:address])

		@order = result.result

		respond_to do |format|
			if result.valid?
				has_checked_out

				format.html { redirect_to order_url(@order), notice: "Order was successfully created." }
				format.json { render :show, status: :created, location: @order }
			else
				format.html { render :new, status: :unprocessable_entity }
				format.json { render json: @order.errors, status: :unprocessable_entity }
			end
		end
	end

	private

		def set_order
			@order = Order.where(id: params[:id], customer: @session.customer).first
		end

		def set_shopping_cart
			@shopping_cart = @session.shopping_cart
		end

		def order_params
			params.fetch(:order, {})
		end
end
