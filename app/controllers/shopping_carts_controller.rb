class ShoppingCartsController < ApplicationController
	before_action :has_shopping_cart, only: %i[ update destroy ]
	before_action :set_shopping_cart, only: %i[ index update destroy ]

	# GET /shopping_carts or /shopping_carts.json
	def index
	end

	# PATCH/PUT /shopping_carts/1 or /shopping_carts/1.json
	def update
		empty = params.permit(:empty)

		if empty[:empty].present?
			@shopping_cart.destroy

			respond_to do |format|
				format.html { redirect_to shopping_carts_url, notice: "Shopping cart was emptied successfully." }
				format.json { head :no_content }
			end
		else
			line_items = shopping_cart_params.permit(line_items: [:quantity, :destroy])
			result = UpdateShoppingCart.run(shopping_cart: @shopping_cart, line_items: line_items[:line_items])

			respond_to do |format|
				if result.valid?
					format.html { redirect_to shopping_carts_url, notice: "Shopping cart was successfully updated." }
					format.json { render :index, status: :ok, location: @session.shopping_cart }
				else
					format.html { render :index, status: :unprocessable_entity }
					format.json { render json: result.errors, status: :unprocessable_entity }
				end
			end
		end
	end

	# DELETE /shopping_carts/1 or /shopping_carts/1.json
	def destroy
		@shopping_cart.destroy

		respond_to do |format|
			format.html { redirect_to shopping_carts_url, notice: "Shopping cart was successfully destroyed." }
			format.json { head :no_content }
		end
	end

	private

		# Use callbacks to share common setup or constraints between actions.
		def set_shopping_cart
			@shopping_cart = @session.shopping_cart
		end

		# Only allow a list of trusted parameters through.
		def shopping_cart_params
			params.fetch(:shopping_cart, {})
		end
end
