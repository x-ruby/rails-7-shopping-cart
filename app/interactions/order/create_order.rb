class CreateOrder < ActiveInteraction::Base
	object :session
	object :customer, class: ActionController::Parameters
	object :address, class: ActionController::Parameters

	validates :customer, :address, presence: true

	def execute
		ActiveRecord::Base.transaction do
			new_customer = nil
			if customer[:id].present?
				new_customer = Customer.find(customer[:id])
				new_customer.update(customer)
			end

			unless new_customer.present?
				new_customer = Customer.new(customer)
			end

			new_address = nil
			if address[:id].present?
				new_address = Address.find(address[:id])
				new_address.update(address)
			end

			unless new_address.present?
				new_address = Address.new({ customer: new_customer }.merge(address))
			end

			order = Order.new(
				customer: new_customer,
				address: new_address,
			)

			session.customer = new_customer

			unless session.save && new_customer.save && new_address.save && order.save
				unless new_customer.valid?
					order.errors.add(:base, :customer, message: "Customer details are invalid")
				end

				unless new_address.valid?
					order.errors.add(:base, :address, message: "Address details are invalid")
				end

				errors.merge!(order.errors)
			end

			order
		end
	end
end
