class UpdateShoppingCart < ActiveInteraction::Base
	object :shopping_cart

	object :line_items, class: ActionController::Parameters

	validates :shopping_cart, :line_items, presence: true

	def execute
		ActiveRecord::Base.transaction do
			shopping_cart.line_items.each do |line_item|
				current_item = line_items[line_item.id.to_s]

				if current_item[:destroy].present?
					line_item.destroy
					shopping_cart.line_items.delete(line_item)
				elsif current_item[:quantity]
					line_item.quantity = current_item[:quantity]
					line_item.save
				end
			end
		end

		unless shopping_cart.save
			errors.merge!(shopping_cart.errors)
		end

		if shopping_cart.line_items.length == 0
			shopping_cart.destroy
		end

		shopping_cart
	end
end
