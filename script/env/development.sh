#!/bin/sh

export RAILS_ENV=development

export DISABLE_DATABASE_ENVIRONMENT_CHECK=0
export RAILS_SERVE_STATIC_FILES=1

set -o allexport
source ./.env
source ./.env.development
set +o allexport
echo "Environment = Development"
echo
