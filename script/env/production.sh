#!/bin/sh

export RAILS_ENV=production

export DISABLE_DATABASE_ENVIRONMENT_CHECK=1
export RAILS_SERVE_STATIC_FILES=1

set -o allexport
source ./.env
source ./.env.production
set +o allexport
echo "Environment = Production"
echo
