#!/bin/sh

export RAILS_ENV=test

export DISABLE_DATABASE_ENVIRONMENT_CHECK=0
export RAILS_SERVE_STATIC_FILES=1

set -o allexport
source ./.env
source ./.env.test
set +o allexport
echo "Environment = Test"
echo
