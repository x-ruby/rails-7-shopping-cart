source ~/.bashrc

# git
alias status="git status $@ && echo - && git rev-parse --abbrev-ref HEAD"
alias push="git push $@"
alias commit="git commit $@"
alias pull="git checkout .idea/workspace.xml ; git pull $@"
alias add="git add $@"
alias addall="git add --all $@"
alias log="git log --no-merges --name-status $@"

# rails
alias bi="bundle install $@"
alias dbrb="rails db:drop db:create db:migrate db:schema:dump db:seed $@"
alias dbrbt="rails db:drop db:create db:migrate db:schema:dump RAILS_ENV=test $@"
alias del_pid="rm -f tmp/pids/server.pid"
alias migrate="rails db:migrate $@"
alias seed="rails db:migrate rails db:seed $@"
alias spec="rails spec $@"
alias pspec="rails parallel:spec $@"
alias pmigrate="rails parallel:prepare parallel:migrate $@"
alias pdbrbt="rails parallel:drop parallel:create parallel:prepare parallel:migrate $@"

# services
alias audit="rails bundle-audit $@"
alias brakeman="rails brakeman -A -l $@"
alias critic="rubycritic $@"
alias guard="rails guard $@"
alias server="rails server -b 0.0.0.0 -p 8001 -e development $@"
alias serverp="rails server -b 0.0.0.0 -p 8001 -e production $@"
alias tailwind="rails tailwindcss:watch $@"

# script
alias env_dev="script/env/development.sh"
alias env_prod="script/env/production.sh"
alias env_test="script/env/test.sh"

alias permissions="
chmod -Rv +x ./bin/ ;
chmod -Rv +x ./script/*.sh ;
chmod -Rv +x ./script/**/*.sh ;
chmod -Rv +x ./script/**/**/*.sh ;
chmod -Rv +x ./script/**/**/**/*.sh
"

# cl
alias cl="
echo -- Command List -- ;
echo Git -- add, addall, commit, pull, push, status, log ;
echo Rails -- bi, dbrb, dbrbt, del_pid, migrate, seed, spec
echo Services -- audit, brakeman, critic, guard, server, serverp, tailwind
echo Script -- env_dev, env_prod, env_test
echo Misc -- permissions"

PS1="\[\`if [[ \$? = "0" ]]; then echo '\e[32m\h\e[0m'; else echo '\e[31m\h\e[0m' ; fi\`:\$PWD\n\$ " # this will change your prompt format
